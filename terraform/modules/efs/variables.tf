variable "name" {}
variable "vpc_id" {}
variable "cidr_block" {}
variable "public_subnets" {}
variable "private_subnets" {}
variable "zones" {}
variable "provider_url" {}
variable "provider_arn" {}

