variable "cluster_version" {}
variable "private_subnets" {}
variable "public_subnets" {}
variable "vpc_id" {}
variable "cidr_block" {}
variable "name" {}
variable "instance_type" {}
variable "desired" {}
variable "max" {}
variable "min" {}
variable "key_path" {}
variable "env" {}

