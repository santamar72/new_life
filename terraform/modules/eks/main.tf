locals {
  ami_types = {
    "t2.small"  = "AL2_x86_64"
    "t2.small" = "AL2_ARM_64"
    "t2.small" = "AL2_x86_64"
  }
}

#Creating EKS Cluster-Role
resource "aws_iam_role" "this" {
  name = "${title(var.name)}EKSCluster"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {
        "Service": "eks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
POLICY
}
#If the role and policies do not exist you can create them using these target points!
resource "aws_iam_role_policy_attachment" "AmazonEKSClusterPolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
  role       = aws_iam_role.this.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKSServicePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSServicePolicy"
  role       = aws_iam_role.this.name
}

#creating eks-cluster
resource "aws_eks_cluster" "this" {
  name     = "${var.name}-eks-cluster"
  role_arn = aws_iam_role.this.arn
  version  = var.cluster_version

  vpc_config {
    subnet_ids = [
      var.private_subnets[0],
      var.private_subnets[1],
      var.private_subnets[2],
      var.private_subnets[3],
      #var.private_subnets[4],
      var.private_subnets[5]
    ]
  }

  depends_on = [aws_iam_role_policy_attachment.AmazonEKSClusterPolicy,
  aws_iam_role_policy_attachment.AmazonEKSServicePolicy]
}
#EKS-Worker_Node_Group
resource "aws_iam_role" "node_group" {
  name = "EKSNodeGroup"

  assume_role_policy = jsonencode({
    Statement = [{
      Action = "sts:AssumeRole"
      Effect = "Allow"
      Principal = {
        Service = "ec2.amazonaws.com"
      }
    }]
    Version = "2012-10-17"
  })
}

#resource "aws_iam_service_linked_role" "eks-nodegroup" {
  #aws_service_name = "eks-nodegroup.amazonaws.com"
#}

resource "aws_iam_role_policy_attachment" "AmazonEKSWorkerNodePolicy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
  role       = aws_iam_role.node_group.name
}

resource "aws_iam_role_policy_attachment" "AmazonEKS_CNI_Policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
  role       = aws_iam_role.node_group.name
}

resource "aws_iam_role_policy_attachment" "AmazonEC2ContainerRegistryReadOnly" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.node_group.name
}
resource "aws_iam_role_policy_attachment" "AmazonElasticFileSystemFullAccess" {
  // @TODO Do we really need to give full access ?
  policy_arn = "arn:aws:iam::aws:policy/AmazonElasticFileSystemFullAccess"
  role       = aws_iam_role.node_group.name
}
resource "aws_iam_role_policy_attachment" "AmazonElasticFileSystemClientFullAccess" {
  // @TODO Do we really need to give full access ?
  policy_arn = "arn:aws:iam::aws:policy/AmazonElasticFileSystemClientFullAccess"
  role       = aws_iam_role.node_group.name
}

# Create an SSH key to get into the worker nodes
data "aws_key_pair" "ansible-key" {
  key_name           = "ansible-key"
  include_public_key = true
}

#resource "aws_key_pair" "myapp" {
  #key_name   = "eks-myapp-${var.env}"
  #public_key = file(var.key_path)
#}

#Creating-EKS-Worker_Node_Group
resource "aws_eks_node_group" "this" {
  cluster_name    = aws_eks_cluster.this.name
  node_group_name = "${var.name}-eks-cluster-node-group"
  node_role_arn   = aws_iam_role.node_group.arn

  # (t3.small) is not supported in your requested Availability Zone (us-east-1e).
  subnet_ids = [
    var.private_subnets[0],
    var.private_subnets[1],
    var.private_subnets[2],
    var.private_subnets[3],
    #var.private_subnets[4],
    var.private_subnets[5]
  ]

  capacity_type  = "ON_DEMAND"
  instance_types = [var.instance_type]
  ami_type       = local.ami_types[var.instance_type]

  scaling_config {
    desired_size = var.desired
    max_size     = var.max
    min_size     = var.min
  }

  update_config {
    max_unavailable = 1
  }

  labels = {
    role = "general"
  }

  # User ec2-user as the username
  remote_access {
    ec2_ssh_key = data.aws_key_pair.ansible-key.key_name
  }

  # taint {
  #   key    = "team"
  #   value  = "devops"
  #   effect = "NO_SCHEDULE"
  # }

  # launch_template {
  #   name    = aws_launch_template.eks-with-disks.name
  #   version = aws_launch_template.eks-with-disks.latest_version
  # }

  depends_on = [
    aws_iam_role_policy_attachment.AmazonEKSWorkerNodePolicy,
    aws_iam_role_policy_attachment.AmazonEKS_CNI_Policy,
    aws_iam_role_policy_attachment.AmazonEC2ContainerRegistryReadOnly,
    aws_iam_role_policy_attachment.AmazonElasticFileSystemFullAccess,
    aws_iam_role_policy_attachment.AmazonElasticFileSystemClientFullAccess
  ]
}

