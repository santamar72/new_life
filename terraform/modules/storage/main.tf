provider "aws" {
  alias = "this"
}

// multi-region
provider "aws" {
  alias = "that"
}

locals {
  graph_api_cache_port = 6379
}

resource "aws_secretsmanager_secret" "trip_db_user" {
  name = "trip-db-user"
}

resource "aws_secretsmanager_secret_version" "trip_db_user" {
  secret_id     = aws_secretsmanager_secret.trip_db_user.id
  secret_string = "trip-api"
}

resource "aws_secretsmanager_secret" "trip_db_pass" {
  name = "trip-db-pass"
}

resource "aws_secretsmanager_secret_version" "trip_db_pass" {
  secret_id     = aws_secretsmanager_secret.trip_db_pass.id
  secret_string = var.trip_db_password
}

resource "aws_secretsmanager_secret" "trip_db_port" {
  name = "trip-db-port"
}

resource "aws_secretsmanager_secret_version" "trip_db_port" {
  secret_id     = aws_secretsmanager_secret.trip_db_port.id
  secret_string = "3306"
}

# https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/Concepts.DBInstanceClass.html#Concepts.DBInstanceClass.Types
# https://aws.amazon.com/rds/aurora/pricing/
resource "aws_rds_global_cluster" "this" {
  provider                  = aws.this
  global_cluster_identifier = "trip-${var.env}"
  engine                    = "aurora-mysql"
  engine_version            = "8.0.mysql_aurora.3.02.0"
  database_name             = "trip"
}

# Standard Instances
# db.t4g.medium 2 vCPUs 4GiB RAM $0.073
# db.t4g.large  2 vCPUs 8GiB RAM $0.146
module "trip_db" {
  source                    = "../mysql-cluster"
  env                       = var.env
  name                      = "trip"
  engine                    = aws_rds_global_cluster.this.engine
  engine_version            = aws_rds_global_cluster.this.engine_version
  aws_rds_global_cluster_id = aws_rds_global_cluster.this.id
  availability_zones        = ["us-east-1a", "us-east-1b", "us-east-1c"] // @TODO
  vpc_id                    = var.vpc_id
  cidr_block                = var.cidr_block
  uname                     = "admin"
  upass                     = var.trip_db_admin_pw
  class                     = "db.r5.large"
  node_count                = 2
  private_subnets           = var.private_subnets
  providers = {
    aws.this = aws.this
  }
}
module "trip_db2" {
  source                    = "../mysql-cluster"
  env                       = var.env
  name                      = "trip"
  engine                    = module.trip_db.engine # We pass this value here to create a dependency
  engine_version            = aws_rds_global_cluster.this.engine_version
  aws_rds_global_cluster_id = aws_rds_global_cluster.this.id
  availability_zones        = ["us-west-2a", "us-west-2b", "us-west-2c"] // @TODO
  vpc_id                    = var.that_vpc_id
  cidr_block                = var.that_cidr_block
  uname                     = "" // Cannot specify user name for cross region replication clusterk
  upass                     = "" // Cannot specify password for cross region replication cluster
  class                     = "db.r5.large"
  node_count                = 1
  private_subnets           = var.that_private_subnets
  providers = {
    aws.this = aws.that // multi-region
  }
}

resource "aws_secretsmanager_secret" "trip_db_host" {
  name = "trip-db-host"
}

resource "aws_secretsmanager_secret_version" "trip_db_host" {
  secret_id     = aws_secretsmanager_secret.trip_db_host.id
  secret_string = module.trip_db.write_endpoint
}

module "graph_api_cache" {
  source                   = "../redis"
  env                      = var.env
  name                     = "graph-api-cache"
  node_count               = 1
  node_type                = "cache.t4g.micro"
  engine                   = "redis"
  engine_version           = "6.2"
  vpc_id                   = var.vpc_id
  subnets                  = var.private_subnets
  parameter_group_name     = "default.redis6.x"
  cpu_threshold            = 80
  bytes_threshold          = 250000000
  maintenance_window       = ""
  apply_immediately        = var.env == "prod" ? false : true
  snapshot_name            = ""
  snapshot_window          = "sun:06:00-sun:08:00"
  snapshot_retention_limit = var.env == "prod" ? 7 : 2
  sns_topic                = var.sns_topic
  port                     = local.graph_api_cache_port
  providers = {
    aws.this = aws.this
  }
}

