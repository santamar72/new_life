variable "env" {}
variable "key_path" {}
variable "sns_topic" {}
variable "zone" {}
variable "vpc_id" {}
variable "that_vpc_id" {}
variable "cidr_block" {}
variable "that_cidr_block" {}
variable "private_subnets" {}
variable "that_private_subnets" {}
variable "availability_zones" {}
variable "trip_db_admin_pw" {
  description = "Database administrator password"
  type        = string
  sensitive   = true
}
variable "trip_db_password" {
  description = "Database password used by services"
  type        = string
  sensitive   = true
}

