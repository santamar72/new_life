#// Route 53 record
#resource "aws_route53_record" "this" {
  #zone_id = var.zone
  #name    = var.dns_name
  #type    = "A"
  #ttl     = "30"
  #records = [aws_instance.this.public_ip]
#}
#resource "aws_route53_record" "this_private" {
  #zone_id = var.zone
  #name    = "${var.dns_name}-private"
  #type    = "A"
  #ttl     = "30"
  #records = [aws_instance.this.private_ip]
#}

// EC2 instance
resource "aws_instance" "this" {
  ami                         = var.ami
  instance_type               = var.class
  subnet_id                   = var.subnets[0]
  vpc_security_group_ids      = [var.security_group]
  associate_public_ip_address = true
  key_name                    = var.key

  tags = {
    Name = var.name
  }
}

resource "aws_security_group" "ssh_sg" {
  name        = "ssh_acces"
  description = "ssh access"
  vpc_id      = var.vpc_id

  ingress {
    description = " ssh access"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    # You can limit this to the cidr_block
    # of the VPC after configuring the VPN
    cidr_blocks = ["0.0.0.0/0"]
    #cidr_blocks = [var.cidr_block]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "SSH_Sg"
  }
}
