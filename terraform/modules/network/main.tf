provider "aws" {
  alias = "this"
}

locals {
  tags = {
    Purpose = "Network"
  }
}

data "aws_availability_zones" "available" {
  provider = aws.this
  state    = "available"
}

resource "aws_vpc" "main" {
  provider             = aws.this
  cidr_block           = "${var.network}.0.0/16"
  enable_dns_hostnames = true
  // We rely on the Name tag to equal var.name
  // so we can find this network in the peering module
  tags = merge(local.tags, map("Name", var.name))
}

resource "aws_eip" "nat" {
  provider = aws.this
  vpc      = true
  tags     = merge(local.tags, map("Name", "${var.name}-NAT-EIP"))
}

resource "aws_nat_gateway" "main" {
  provider      = aws.this
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.primary.id
  tags          = merge(local.tags, map("Name", "${var.name}-NAT"))
}

resource "aws_internet_gateway" "main" {
  provider = aws.this
  vpc_id   = aws_vpc.main.id
  tags     = merge(local.tags, map("Name", "${var.name}-IGW"))
}

resource "aws_default_route_table" "default" {
  provider               = aws.this
  default_route_table_id = aws_vpc.main.default_route_table_id
  tags                   = merge(local.tags, map("Name", "${var.name}-Default"))
}

resource "aws_route_table" "primary" {
  provider = aws.this
  vpc_id   = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = merge(local.tags, map("Name", "${var.name}-Primary"))
}

resource "aws_route_table" "public" {
  provider = aws.this
  vpc_id   = aws_vpc.main.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.main.id
  }

  tags = merge(local.tags, map("Name", "${var.name}-Public"))
}

resource "aws_route_table" "private" {
  provider = aws.this
  vpc_id   = aws_vpc.main.id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.main.id
  }

  tags = merge(local.tags, map("Name", "${var.name}-Private"))
}

resource "aws_subnet" "primary" {
  provider          = aws.this
  vpc_id            = aws_vpc.main.id
  cidr_block        = "${var.network}.240.0/20"
  availability_zone = data.aws_availability_zones.available.names[length(data.aws_availability_zones.available.names) - 1]
  tags              = merge(local.tags, map("Name", "${var.name}-Primary"))
}

resource "aws_route_table_association" "primary" {
  provider       = aws.this
  subnet_id      = aws_subnet.primary.id
  route_table_id = aws_route_table.primary.id
}

resource "aws_subnet" "private" {
  provider          = aws.this
  count             = length(data.aws_availability_zones.available.names)
  vpc_id            = aws_vpc.main.id
  cidr_block        = "${var.network}.${16 * count.index}.0/20"
  availability_zone = data.aws_availability_zones.available.names[count.index]
  tags              = merge(local.tags, map("Name", "${var.name}-Private"))
}

resource "aws_route_table_association" "private" {
  provider       = aws.this
  count          = length(data.aws_availability_zones.available.names)
  subnet_id      = aws_subnet.private[count.index].id
  route_table_id = aws_route_table.private.id
}

resource "aws_subnet" "public" {
  provider                = aws.this
  count                   = length(data.aws_availability_zones.available.names)
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "${var.network}.${16 * count.index + 112}.0/20"
  availability_zone       = data.aws_availability_zones.available.names[count.index]
  tags                    = merge(local.tags, map("Name", "${var.name}-Public"))
  map_public_ip_on_launch = true
}

resource "aws_route_table_association" "public" {
  provider       = aws.this
  count          = length(data.aws_availability_zones.available.names)
  subnet_id      = aws_subnet.public[count.index].id
  route_table_id = aws_route_table.public.id
}
