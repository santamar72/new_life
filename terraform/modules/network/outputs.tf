output "vpc" {
  value = aws_vpc.main.id
}

output "cidr_block" {
  value = aws_vpc.main.cidr_block
}

output "primary_subnet" {
  value = aws_subnet.primary.id
}

output "private_subnets" {
  value = aws_subnet.private[*].id
}

output "public_subnets" {
  value = aws_subnet.public[*].id
}

output "availability_zones" {
  value = data.aws_availability_zones.available.names
}
