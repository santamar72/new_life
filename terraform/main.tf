locals {
  env = var.environments[terraform.workspace]
}
provider "aws" {
  alias = "current"
}

#module "ec2_instance" {
  #source              = "./modules/ec2-instance/"
  #ami                 = var.ami
  #class               = var.class
  #subnets             = module.network.public_subnets
  #vpc_id              = module.network.vpc
  #public_key_location = var.public_key_location
  #name                = var.name
  #zone                = module.network.availability_zones
  #env                 = local.env
  #environments        = var.environments[terraform.workspace]
#}

# VPC subnets NAT IGW
module "network" {
  source  = "./modules/network"
  name    = "${title(var.environments[terraform.workspace])}Network"
  network = var.networks[terraform.workspace]
  env     = local.env
  providers = {
    aws.this = aws.current
  }
}
#EKS-CLUSTER
module "eks" {
  source          = "./modules/eks"
  env             = local.env
  name            = var.name # Name is coupled to CI process
  cluster_version = var.cluster_version
  key_path        = var.public_key_location
  private_subnets = module.network.private_subnets
  public_subnets  = module.network.public_subnets
  vpc_id          = module.network.vpc
  cidr_block      = module.network.cidr_block
  instance_type   = local.env == "dev" ? "t2.small" : "t2.small"
  desired         = local.env == "dev" ? 1 : 1
  max             = local.env == "dev" ? 1 : 1
  min             = local.env == "dev" ? 1 : 1
}

