# As general good practice, any secret added here should
# be pushed to AWS Secrets Manager via terraform resources.
# This way the next developer can get it from there and so
# can our CI/CD processes etc.
#
# Copy this file for each env you're using e.g:
# `cp secret.dev.tfvars-dist secret.dev.tfvars`
# Be sure to replace the values here with the
# correct ones.

trip_db_password = "get the real password from AWS Secrets manager"

