variable "region" {
  type    = string
  default = "us-east-1"
}

#variable "region2" {
  #type    = string
  #default = "us-west-2"
#}

// This variable allows us to map workspaces
// to the AWS account we want to launch resources
// in.
variable "aws_profiles" {
  type = map(string)

  default = {
    "default" = "new-life"
    "dev"     = "new-life"
    #"test"    = "sky-test"
    #"stage"   = "sky-stage"
    #"prod"    = "sky-prod"
  }
}

#variable "class" {
  #type    = string

  #default = {
    #"default" = "new-life"
    #"dev"     = ""
    #"test"    = ""
    #"stage"   = ""
    #"prod"    = ""
  #}
#}
variable "public_key_location" { 
    type          = string
    default       = "~/.ssh/ansible-key.pem"
    description   = "my_public_key"
  }

#variable "security_group" {
  #type    = string

  #default = ""
#}

variable "cluster_version" {
  type    = string
  default = "1.23"
}
variable "ami" {
  type     = string
  #default  = "ami-00874d747dde814fa"
  default  = "ami-00874d747dde814fa"
}

variable "name" {
   type  = string
   default = "my_app"
}
// This variable allows us to map workspaces
// to the environment we want to use.
// We usually append the environment to the
// names of resources for clarity AND because
// in some cases we need to create a resource
// under a different environment but in the same
// AWS account without AWS complaining the resources
// already exists.
//
// If two keys have the same value, make sure
// they dont both also correlate to the same
// profile i.e. var.aws_profiles or else you
// defeat the above purpose.
variable "environments" {
  type = map(string)

  default = {
    "default" = "main"
    "dev"     = "dev"
    "test"    = "test"
    "stage"   = "stage"
    "prod"    = "prod"
  }
}

variable "networks" {
  type = map(string)

  default = {
    "default"  = "10.150"
    "dev"     = "10.140"
    #"test"    = "10.130"
    #"stage"   = "10.120"
    #"prod"    = "10.110"
  }
}

#variable "networks2" {
  #type = map(string)

  #default = {
    #"default" = "10.250"
    #"dev"     = "10.240"
    #"test"    = "10.230"
    #"stage"   = "10.220"
    #"prod"    = "10.210"
  #}
#}

# List of accounts that would be allowed
# to push/pull from ECR, what account IDs
# can push to SNS topic for alerts etc.
#variable "account_ids" {
  #type = map(string)

  #default = {
    #"main"  = "971248152716"
    #"dev"   = "391129331375"
    #"test"  = ""
    #"stage" = "236980769029"
    #"prod"  = "553962435188"
  #}
#}

# Populate this after you figure out the
# external IP for your ingress
#variable "external_ip_ingress" {
  #type = map(string)

  #default = {
    #"dev"   = ""
    #"test"  = ""
    #"stage" = ""
    #"prod"  = "aaae2084a6f5743af8a3588fc042538e-919723407.us-east-1.elb.amazonaws.com"
  #}
#}

#variable "external_ip_chat" {
  #type = map(string)

  #default = {
    #"dev"   = ""
    #"test"  = ""
    #"stage" = ""
    #"prod"  = "aaae2084a6f5743af8a3588fc042538e-919723407.us-east-1.elb.amazonaws.com"
  #}
#}

# After configuring a VPN you can optionally
# take an AMI. If you decide to destroy the VPN,
# set the AMI ID here so TF can rebuild it from
# the AMI this way you don't have to reconfigure
# the VPN and redistribute new credentials.
#variable "configured_amis_for_vpn" {
  #type = map(string)

  #default = {
    #"default" = ""
    #"dev"     = "ami-06cd11ee92d5a6913"
    #"test"    = ""
    #"stage"   = "ami-0c9a87b0ca0ca3efd"
    #"prod"    = ""
  #}
#}

## users
#variable "users" {
  #type    = list(any)
  #default = ["dstephenson", "josue", "carmstrong", "skyhoptech", "alouisdhon"]
#}

#variable "db_user_names" {
  #type    = list(string)
  #default = ["user", "manifest", "trip", "location", "contract", "invoice", "airport"]
#}

## -------------------------------------------
##                 secrets
## secrets should be defined in a .tfvars file
## at the root of this repo. this file is not
## checked into source control.
## -------------------------------------------
#variable "trip_db_admin_pw" {
  #description = "database administrator password"
  #type        = string
  #sensitive   = true
#}

#variable "trip_db_password" {
  #description = "database password used by services"
  #type        = string
  #sensitive   = true
#}

#variable "flightstats_app_id" {
  #description = "flightstats app id"
  #type        = string
  #sensitive   = true
#}

#variable "flightstats_app_key" {
  #description = "flightstats app key"
  #type        = string
  #sensitive   = true
#}

#variable "flightaware_api_key" {
  #description = "flightaware api key"
  #type        = string
  #sensitive   = true
#}
