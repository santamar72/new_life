variable "instance_count" {
  default = 2
}
variable "ami" {
    type          = string
    #default       = "ami-0557a15b87f6559cf"#ubuntu
    default       = "ami-006dcf34c09e50022"
    description   = "ami-type"
}
variable "instance_type" {
    type          = string
    default       = "t2.micro"
    description   = "instance_type"
}
variable "vpc_cidr_block" {
    type          = string
    default       = "10.0.0.0/16"
    description   = "vpc cidr block"
}
variable "subnet_cidr_block" {
    type          = string
    default       = "10.0.20.0/24"
    description   = "subnet cidr block"
}
variable "avail_zone" {
    type          = string
    default       = "us-east-1a"
    description   = "az zones"
}
variable "env_prefix" {
    type          = string
    default       = "dev"
    description   = "environment"
}
variable "my_ip" {
    type          = list
    default       = ["99.137.85.241/32", "108.89.209.208/32"]
    description   = "my_ip"
}
variable "public_key_location" { 
    type          = string
    default       = "~/.ssh/id_rsa.pub"
    description   = "my_public_key"
  }
variable "ssh_private_key" { 
    type          = string
    default       = "~/.ssh/ansible-key.pem"
    description   = "my_private_key"
  }
variable "ssh_fingerprint_key" { 
    type          = string
    default       = "44:d7:37:f6:54:b1:79:68:57:7c:04:a1:d2:f0:6c:68"
    description   = "my_fingerprint_key"
  }
