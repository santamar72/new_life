# Arguments to pass to terraform commands
T_ARGS ?=  \

#-target module.state \
#-target module.ecr \
#-target module.iam \
#-target module.billing \
#-target module.dns \
#-target module.network \
#-target module.network2 \
#-target module.vpn \
#-target module.ips \
#-target module.sns \
#-target module.eks \
#-target module.oidc \
#-target module.efs \
#-target module.storage \
#-target module.services \
#-target module.portal \
#-target module.ses \

#-no-color \

AWS_REGION ?= us-east-1
AWS_PROFILE ?= new-life

# Always prefix our terraform commands with the sky profile
# and allow the config.tf to select the appropriate profile
terraform = AWS_SDK_LOAD_CONFIG=1 AWS_PROFILE=$(AWS_PROFILE) terraform
WSPACE = $(shell $(terraform) workspace list | grep '*' | awk '{print $$2}')
aws_prof = new-life-$(WSPACE)
AWS_CMD = AWS_PROFILE=$(AWS_PROF) aws

RED=\033[0;31m
GREEN=\033[0;32m
NC=\033[0m # No Color

.PHONY: default
default: help

.PHONY: init
init: ## Configure the backend for remote storage of state files
	$(terraform) init \
 && $(terraform) workspace new dev; \
	$(terraform) workspace select dev # There should be little reason to leave the dev workspace

# Plan out infrastructure
.PHONY: plan
plan: tplan show ## Create terraform plan and show target info

.PHONY: tplan
tplan: ## Create terraform plan
	$(terraform) plan $(T_ARGS) -var-file="secret.$(WSPACE).tfvars" -out=tplan

.PHONY: apply
apply: ## Apply terraform plan then remove the plan
	$(terraform) apply tplan && rm tplan

.PHONY: destroy
destroy: show ## Destory terraform managed resources
	$(terraform) plan -destroy $(T_ARGS) -var-file="secret.$(WSPACE).tfvars" -out=tplan

.PHONY: taint
taint: ## Taint a resource from the given target\n   target= module.apps.module.trips
	@if [ -z $(target) ]; then echo "a target is required"; exit 1; fi;
	$(terraform) taint $(target)

.PHONY: state
state: ## Sync the remote state from S3 to local
	AWS_PROFILE=$(AWS_PROFILE) aws s3 sync s3://io.skyvibe.terraform/ ./state/s3/

.PHONY: push
push: ## Sync the local state from local to S3
	AWS_PROFILE=$(AWS_PROFILE) aws s3 sync ./state/s3/ s3://io.skyvibe.terraform/

.PHONY: restart
restart:
	make destroy && make apply && make plan && make apply


.PHONY: show
show: ## Desplay the current terraform workspace

	@echo "TF Workspace: $(GREEN)$(WSPACE)$(NC)"
	@echo "AWS Profile: $(GREEN)$(AWS_PROF)$(NC)"
	@echo "Target: $(GREEN)$(T_ARGS)$(NC)"

.PHONY: help
help: ## this help
	# Usage:
	#   make <target> [OPTION=value]
	#
	# Targets:
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {sub("\\\\n",sprintf("\n%22c"," "), $$2);printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)
