provider "aws" {
  region     = "us-east-1"
  profile    = "new-life"
}
resource "aws_vpc" "myapp_vpc" {
    cidr_block = var.vpc_cidr_block

    tags       = {
      Name     = "${var.env_prefix}-vpc"
  }
}
resource "aws_subnet" "myapp_subnet-1" {
    vpc_id            = aws_vpc.myapp_vpc.id
    cidr_block        = var.subnet_cidr_block
    availability_zone = var.avail_zone

    tags   = {
      Name = "${var.env_prefix}-subnet-1"
  }
}
#don't activate this line downti 32
##resource "aws_route_table" "myapp_route_table" {
  ##vpc_id = aws_vpc.myapp_vpc.id

  ##route {
    ##cidr_block = "0.0.0.0/0"
    ##gateway_id = aws_internet_gateway.myapp_igw.id
  ##}
    ##tags   = {
      ##Name = "${var.env_prefix}-rtb"
  ##}
##}
resource "aws_internet_gateway" "myapp_igw" {
  vpc_id = aws_vpc.myapp_vpc.id
    tags   = {
      Name = "${var.env_prefix}-igw"
  }
}
#resource "aws_route_table_association" "assoc_rtb" {
    #subnet_id      = aws_subnet.myapp_subnet-1.id
    #route_table_id = aws_route_table.myapp_route_table.id
#}
resource "aws_default_route_table" "default-rtb" {
    default_route_table_id = aws_vpc.myapp_vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp_igw.id
  }
    tags   = {
      Name = "${var.env_prefix}-main-rtb"
  }
}

resource "aws_default_security_group" "default-sg" {
      vpc_id = aws_vpc.myapp_vpc.id

      ingress {
          from_port = 22
          to_port   = 22
          protocol  = "tcp"
          cidr_blocks = var.my_ip
      }

      ingress {
          from_port = 8080
          to_port   = 8080
          protocol  = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
      }

      ingress {
          from_port = 8081
          to_port   = 8081
          protocol  = "tcp"
          cidr_blocks = ["0.0.0.0/0"]
      }

      egress {
          from_port = 0
          to_port   = 0
          protocol  = "-1"
          cidr_blocks = ["0.0.0.0/0"]
          prefix_list_ids = []
      }
    tags   = {
      Name = "${var.env_prefix}-default-sg"
  }
}

data "aws_key_pair" "ansible-key" {
  key_name           = "ansible-key"
  include_public_key = true
}
#resource "aws_key_pair" "ssh_key_pair" {
  #key_name   = "server_key"
  #public_key = file(var.public_key_location)
#}


output "ec2_public_ip" {
    value = [aws_instance.myapp_server.*.public_ip]
}

resource "aws_instance" "myapp_server" {
    count                  = var.instance_count
    ami                    = var.ami
    instance_type          = var.instance_type
    subnet_id              = aws_subnet.myapp_subnet-1.id
    vpc_security_group_ids = [aws_default_security_group.default-sg.id]
    availability_zone      = var.avail_zone

    associate_public_ip_address = true
    key_name      = data.aws_key_pair.ansible-key.key_name

    #user_data = file("entry-script.sh")

    provisioner "local-exec" {
      working_dir = "/Users/johnsant/Clients/john/repos/new_life/ansible/"
      command     = "sleep 300 && ansible-playbook --inventory ${self.public_ip}, --private-key ${var.ssh_private_key} -u ec2-user deploy-docker.yaml"
    }
      tags        = {
        Name      = "${var.env_prefix}-${count.index + 1}-server"
    }
}
#-${count.index + 1}
