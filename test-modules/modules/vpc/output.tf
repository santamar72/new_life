#output "ec2_public_ip" {
    #value = aws_instance.myapp_server[count.index]
#}

output "ec2_public_ip" {
  value = [for instance in aws_instance.myapp_server : instance.public_ip]
}
