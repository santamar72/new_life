loop {
  check server
  if server is good {
    ansible-playbook --inventory ${self.public_ip}, --private-key ${var.ssh_private_key} -u ec2-user deploy-docker.yaml
  }
}
