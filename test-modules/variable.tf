variable "vpc_cidr_block" {
    type          = string
    default       = "10.0.0.0/16"
    description   = "vpc cidr block"
}
variable "subnet_cidr_block" {
    type          = string
    default       = "10.0.50.0/24"
    description   = "subnet cidr block"
}
variable "availability_zone" {
    type          = string
    default       = "us-east-1a"
    description   = "az zones"
}
