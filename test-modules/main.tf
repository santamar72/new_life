provider "aws" {
  region     = "us-east-1"
  profile = "new-life"
}

resource "aws_vpc" "dev_vpc" {
    cidr_block = var.vpc_cidr_block

    tags ={
      Name = "development",
      vpc_env = "dev"
 }
}

resource "aws_subnet" "dev-subnet-1" {
    vpc_id = aws_vpc.dev_vpc.id
    cidr_block = var.subnet_cidr_block
    availability_zone = var.availability_zone

    tags ={
      Name = "subnet-1-dev"
 }
}
